wget https://raw.githubusercontent.com/jbrooksuk/node-summary/browser/lib/summary.js -O src/dependencies/summary.js
rm -rf src/dependencies/underscore.js
cp node_modules/underscore/underscore.js src/dependencies/underscore.js
rm -rf src/dependencies/Readability.js
cp node_modules/readability/Readability.js src/dependencies/Readability.js
rm -rf src/dependencies/purify.min.js
cp node_modules/dompurify/dist/purify.min.js src/dependencies/purify.min.js