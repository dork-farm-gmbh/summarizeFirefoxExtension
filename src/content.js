var documentClone = document.cloneNode(true); 
var article = new Readability(documentClone).parse();
browser.runtime.sendMessage({
    type: "article",
    body: article
});