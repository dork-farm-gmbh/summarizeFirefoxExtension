function renderResults(title, summary) {
    document.getElementById('title').textContent = title;
    document.getElementById('summary').innerHTML = DOMPurify.sanitize(summary);
}

function handleMessage(request) {
    let summary = request.body.excerpt
    Summary.summarize(request.body.title, request.body.content, (err, content) => {
        if(err && !summary.length) {
            summary = 'Failed to generate summary...'
        } else if (content.length > request.body.excerpt.length) {
           summary = content;
        }
        renderResults(request.body.title, summary);
    });
}

browser.runtime.onMessage.addListener(handleMessage);

async function injectContentScripts() {
    const tabs = await browser.tabs.query({
        active: true,
        currentWindow: true
    });

    var tab = tabs[0];
    await browser.tabs.executeScript(tab.id, { file: 'dependencies/Readability.js' });
    await browser.tabs.executeScript(tab.id, { file: 'content.js' });
}

injectContentScripts();